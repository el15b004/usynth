`timescale 1ns/100ps

module tb_pwm ();

    reg clock;
    reg ce25;
    reg reset;
    wire out_en;
    wire out_sam;
    wire out_pwm;
    wire out;

    clken U1(clock,reset,ce25,out_en,out_sam,out_pwm);

    pwm U0(clock,reset,out_pwm,8'h0A,out);

    initial begin
        clock     = 1'b0;
        ce25      = 1'b0;
        reset     = 1'b1;
        #10;
        reset     = 1'b0;
        #10000 $finish;
    end





    always
          #5 clock = ~clock;

    always
          #20 ce25 = ~ce25;


          initial begin
          $display("clk out");
          $monitor("CLK:%b,reset:%b,PWM_EN:%b,PWM_OUT:%b",clock,reset,out_pwm,out);
        end


        integer f;
        initial begin
        f = $fopen("out_pwm.csv "); if(!f) $finish;
        end
        always @(posedge clock)
        $fdisplay(f, "%d",out);

endmodule
