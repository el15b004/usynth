`timescale 1ns/100ps

module tb_spi2bus_top ();

    reg clock;
    reg reset;
    reg mosi;
    reg sck;
    reg ss;
    wire [23:0] rxdata;
    wire rxen;
    wire [15:0] o_address;
    wire [7:0] o_data;
    wire o_write;


 // spi master
    spi2bus_top U0(clock,reset,mosi,sck,ss,rxdata,rxen,o_address,o_data,o_write);

    initial begin
        clock     = 1'b0;
        reset     = 1'b1;
        ss        = 1'b0;
        sck       = 1'b0;
        mosi      = 1'b0;
        #10;
        reset     = 1'b0;
        #10000 $finish;
    end


    always
          #400 mosi = ~mosi;

    always
          #100 sck = ~sck;

    always
          #5 clock = ~clock;



          initial begin
          $display("spi2bus out");
          $dumpfile("spi2bus.vcd");
          $dumpvars(0,tb_spi2bus_top);
          //$monitor("clk:%d,sck:%d,mosi :%d, data:%d",clock, sck,mosi,rxdata);
        end

endmodule
