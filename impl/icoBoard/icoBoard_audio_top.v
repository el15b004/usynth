module hx8k_audio_top #(parameter NUM_VOICES = 3, parameter WAVEFORM = 0) ( input  clock_in_100,
                    input ser_in,
                    input  sck,
                    input  mosi,
                    input  ss,
                    output gain, // 1: 6dB, 0: 12dB
                    output shutdown, // 1: on, 0: off
                    output [7:0] led,
                    output pwm,
                    output ser_out);

    reg [NUM_VOICES*16-1:0] freq;
    reg [NUM_VOICES*16-1:0] a;
    reg [NUM_VOICES*16-1:0] d;
    reg [NUM_VOICES*16-1:0] s;
    reg [NUM_VOICES*16-1:0] r;


    assign shutdown = 1'b1;
    assign gain     = 1'b1;

    wire locked;
    wire reset;
    reg [2:0] reset_sync;
    wire clock;
    reg [7:0] led_reg;
    wire mon_ena;
    wire [7:0] mon_led;

    reg [7:0] i;
    reg [7:0] j;
    reg [NUM_VOICES-1:0] midi_gates;
    reg [NUM_VOICES-1:0] play_note;
    reg [NUM_VOICES-1:0] recieved_play_note;
    reg [NUM_VOICES-1:0] envelope;
    reg [NUM_VOICES-1:0] enable;
    reg [NUM_VOICES*8-1:0] freq_l;
    reg [NUM_VOICES*8-1:0] a_l;
    reg [NUM_VOICES*8-1:0] d_l;
    reg [NUM_VOICES*8-1:0] s_l;
    reg [NUM_VOICES*8-1:0] r_l;

    pll clkgen (.clock_in(clock_in_100),.clock_out(clock),.locked(locked));

    initial
      reset_sync <= 3'b1;

    always @(posedge clock or negedge locked) begin
        if (!locked) begin
            reset_sync <= 3'b1;
        end else begin
            reset_sync <= {reset_sync[1:0],1'b0};
        end
    end

    assign reset = reset_sync[2];

	wire [7:0]gate;
  reg [8*16-1:0] MIDIfreq;

  reg [7:0]MIDILED;



    top_midi_uart tmu1
     (.clock(clock),
      .reset(reset),
      .MIDILED(MIDILED),
      .ser_in(ser_in),
      .gate(gate),
      .freq(MIDIfreq[NUM_VOICES*16-1:0]),
      .ser_out(ser_out)
    );

    usynth #(.NUM_VOICES(NUM_VOICES), .WAVEFORM(WAVEFORM)) syn
                (.clock(clock), .ce25(1), .reset(reset),
                .play_note(play_note[NUM_VOICES-1:0]),
                .enable(enable[NUM_VOICES-1:0]),
                .freq(freq[NUM_VOICES*16-1:0]),
                .pwm(pwm),
                .a(a[NUM_VOICES*16-1:0]),
                .d(d[NUM_VOICES*16-1:0]),
                .s(s[NUM_VOICES*16-1:0]),
                .r(r[NUM_VOICES*16-1:0]),
                .envelope(envelope[NUM_VOICES-1:0]));




    wire [15:0] address;
    wire [7:0]  data;
    wire wr;

    spi2bus_top i_spi (.clock(clock),.reset(reset),
                         .mosi(mosi),.ss(ss),.sck(sck),
                         .int_address(address),
                         .int_wr_data(data),
                         .int_write(wr));


    assign led = {2'b0,play_note};

    //assign led = {2'b0, play_note & enable};

    always @(posedge(clock) or posedge(reset))
    begin
        if (reset) begin
            led_reg <= 8'hff;
            //MIDILED <= 8'h00;
            for (i=0;i<NUM_VOICES;i=i+1) begin
                midi_gates     <= 0;
                freq_l[i*8+:8] <= 0;
                a_l[i*8+:8]     <= 0;
                d_l[i*8+:8]     <= 0;
                s_l[i*8+:8]     <= 0;
                r_l[i*8+:8]     <= 0;
                freq [i*16+:16] <= 440;
                a[i*16+:16]     <= 16'h400;
                d[i*16+:16]     <= 16'h400;
                s[i*16+:16]     <= 16'h8000;
                r[i*16+:16]     <= 16'h0100;
                enable          <= 0;
                play_note       <= 0;
                recieved_play_note <= 0;
                envelope        <= 0;
            end
        end else begin
            play_note <= (midi_gates &  gate) | recieved_play_note;
            for (j=0;j<NUM_VOICES;j=j+1) begin
              if(midi_gates[j] == 1'b1) begin
                freq[15+j*16:j*16] <= MIDIfreq[15+j*16:j*16];



                //play_note <= midi_gates;
              end
            end
            if (mon_ena)
               led_reg <= mon_led;
            if (wr)
                if (address > 16'hFF) begin
                        enable[address[3:0]]       <= data[0];
                        recieved_play_note[address[3:0]]    <= data[1];
                        envelope[address[3:0]]     <= data[2];
                        midi_gates[address[3:0]]   <= data[3];
                end else begin
                    case (address[3:0])
                        4'h0: freq_l[address[7:4]*8 +:8]  <= data;
                        4'h1: freq  [address[7:4]*16+:16] <= {data, freq_l[address[7:4]*8+:8]};
                        4'h2: a_l   [address[7:4]*8 +:8]  <= data;
                        4'h3: a     [address[7:4]*16+:16] <= {data, a_l[address[7:4]*8 +:8]};
                        4'h4: d_l   [address[7:4]*8 +:8]  <= data;
                        4'h5: d     [address[7:4]*16+:16] <= {data, d_l[address[7:4]*8 +:8]};
                        4'h6: s_l   [address[7:4]*8 +:8]  <= data;
                        4'h7: s     [address[7:4]*16+:16] <= {data, s_l[address[7:4]*8 +:8]};
                        4'h8: r_l   [address[7:4]*8 +:8]  <= data;
                        4'h9: r     [address[7:4]*16+:16] <= {data, r_l[address[7:4]*8 +:8]};
                        default : play_note <= play_note;
                    endcase
                end
        end

    end
endmodule
