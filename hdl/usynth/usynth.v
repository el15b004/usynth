module usynth #(parameter NUM_VOICES = 3, parameter WAVEFORM = 0)
             (input wire clock,
              input wire ce25,
              input wire reset,
              input wire [NUM_VOICES-1:0] enable,
              input wire [NUM_VOICES-1:0] play_note,
              input wire [NUM_VOICES*16-1:0] freq,
              output wire pwm,
              input wire [NUM_VOICES*16-1:0] a,
              input wire [NUM_VOICES*16-1:0] d,
              input wire [NUM_VOICES*16-1:0] s,
              input wire [NUM_VOICES*16-1:0] r,
              input wire [NUM_VOICES-1:0] envelope);


    wire ce_sample;
    wire ce_envelope;
    wire ce_pwm;

    wire [15:0] sample_out [NUM_VOICES-1:0];
    wire [15:0] sample [NUM_VOICES-1:0];

    reg  [15:0] sum;

    clken ce (.clock(clock),.reset(reset),
               .ce25(ce25),
               .ce_envelope(ce_envelope),
               .ce_sample(ce_sample),
               .ce_pwm(ce_pwm));

    genvar index;
    integer i;

    generate
    for (index=0; index<NUM_VOICES; index=index+1) begin : gen_voice
        voice #(.WAVEFORM(WAVEFORM)) v (.clock(clock),
                  .reset(reset),
                  .ce_sample(ce_sample),
                  .ce_timing(ce_envelope),
                  .freq(freq[index*16+:16]),
                  .a(a[index*16+:16]),
                  .d(d[index*16+:16]),
                  .s(s[index*16+:16]),
                  .r(r[index*16+:16]),
                  .play_note(play_note[index]),
                  .sample(sample_out[index]),
                  .envelope_enable(envelope[index]));
        assign sample[index] = (enable[index]) ? sample_out[index] : 16'h0;
    end
    endgenerate

    always @(*) begin
        sum = 16'h0;
        for (i = 0; i < NUM_VOICES; i=i+1)
            sum = sum + {4'b0,sample[i][15:4]};
    end

    pwm p     (.clock(clock), .reset(reset),
               .ce_pwm(ce_pwm), .sample(sum[15:8]),
               .out(pwm));

endmodule


