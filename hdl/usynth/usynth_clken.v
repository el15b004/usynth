module clken (input  wire clock,
              input  wire reset,
              input  wire ce25,
              output  wire [3:0]en_cnt, //test
              output  wire [11:0]sam_cnt, // test
              output reg  ce_envelope,
              output reg  ce_sample,
              output reg  ce_pwm);

    reg [11:0] sample_count;
    reg [3:0] envelope_count;


    wire ce_envelope_int;
    wire ce_sample_int;
    wire ce_pwm_int;

    localparam SAMPLE_RELOAD   = 12'hB71;
    localparam ENVELOPE_RELOAD = 4'b1111;

    assign ce_pwm_int = ce25;

    assign ce_sample_int = ce25 & ce_pwm_int & (sample_count == 12'b0) ? 1'b1 : 1'b0;

    always @ (posedge clock or posedge reset)
    begin
        if (reset == 1'b1) begin
            sample_count   <= SAMPLE_RELOAD;
        end else if (ce_pwm_int) begin
            sample_count <= sample_count - 1;
            if (sample_count == 12'b0) begin
                sample_count <= SAMPLE_RELOAD;
            end
        end
    end

    assign ce_envelope_int = ce25 & ce_pwm_int & ce_sample_int & (envelope_count == 8'b0) ? 1'b1 : 1'b0;

    always @ (posedge clock or posedge reset)
    begin
        if (reset == 1'b1) begin
            envelope_count   <= ENVELOPE_RELOAD;
        end else if (ce_sample_int) begin
            envelope_count <= envelope_count - 1;
            if (envelope_count == 4'b0) begin
                envelope_count <= ENVELOPE_RELOAD;
            end
        end
    end

    always @(posedge clock or posedge reset)
    begin
        if (reset == 1'b1) begin
            ce_envelope <= 1'b0;
            ce_sample   <= 1'b0;
            ce_pwm      <= 1'b0;
        end else begin
            ce_envelope <= ce_envelope_int;
            ce_sample   <= ce_sample_int;
            ce_pwm      <= ce_pwm_int;
        end
    end

    assign en_cnt = envelope_count;

    assign sam_cnt = sample_count;

endmodule
