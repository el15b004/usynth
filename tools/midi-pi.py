#!/usr/bin/env python2
import mido
import serial
import sys
import time
import signal
import spidev

def get_first_voice(voices,note=None):
    for i in range(0,len(voices)):
        if voices[i] == note:
            return i
    return None



def set_freq(port,voice,freq):
    write(port,0x10*(voice),freq%256)
    write(port,0x10*(voice)+1,freq/256)

def set_adsr(port,voice,a,d,s,r):
	write(port,0x10*(voice)+2,a%256)
	write(port,0x10*(voice)+3,a/256)
	write(port,0x10*(voice)+4,d%256)
	write(port,0x10*(voice)+5,d/256)
	write(port,0x10*(voice)+6,s%256)
	write(port,0x10*(voice)+7,s/256)
	write(port,0x10*(voice)+8,r%256)
	write(port,0x10*(voice)+9,r/256)


def note_on(port,voice):
    write(port,0x100+voice,7)

def note_off(port,voice):
    write(port,0x100+voice,5)

def freq(note):
    return (2.0**((note - 69)/12.0))*440.0;

def sigint_handler(num,frame):
    for v in range(0,8):
       note_off(port,v)
    port.close()
    sys.exit(0)

def write(port,address,byte):

    if address > 65535:
        raise ValueError("Out of address space")

    data = [address/256,address%256,byte]

    print data

    port.xfer2(data,32000,1000,8)

    time.sleep(0.01)

    return True

if __name__=="__main__":

    if len(sys.argv) != 3:
        print "Usage: %s <MIDI FILE> <Number of keyboard voices>" % sys.argv[0]
        sys.exit(-1)

    try:
        port = spidev.SpiDev()
        port.open(0,1)
    except Exception as e:
        port.close()

        sys.stderr.write("Port cannot be opened: ")#%s\n", str(e)")
        sys.exit(-1)

    #signal.signal(signal.SIGINT,sigint_handler)




    for v in range(0,8):
       set_adsr(port,v,0x4000,0x400,0x4000,0x100)
       note_off(port,v)
       set_freq(port,v,440)



    mid = mido.MidiFile(sys.argv[1])
    for msg in mid.play():
        if msg.type == "note_on":
            if (msg.velocity > 0):
                idx = get_first_voice(voices,None)
                if idx is None:
                    print "WARN: no free voices"
                else:
                    voices[idx] = msg.note
                    set_freq(port,idx,int(freq(msg.note)))
                    note_on(port,idx)
                    print "%d on (%d hz)" % (idx, int(freq(msg.note)))
            else:
                idx = get_first_voice(voices,msg.note)
                if idx is None:
                    print "WARN: no note assigned"
                else:
                    voices[idx] = None
                    print "%d off" % idx
                    note_off(port,idx)
        elif msg.type == "note_off":
            idx = get_first_voice(voices,msg.note)
            if idx is None:
                print "WARN: no note assigned"
            else:
                voices[idx] = None
                print "%d off" % idx
                note_off(port,idx)
    port.close()
