`timescale 1ns/1ps

module tb_MIDI_UART ();

    reg clock;
    reg reset;
    reg IN;
    wire [8*15:0] freq;
    wire [7:0] gate;
    //reg [15:0] freq [7:0];

    top_midi_uart U0(clock,reset,IN,gate,freq);

    initial begin
        clock     = 1'b0;
        reset     = 1'b1;
        IN        = 8'b0;
        #10;
        reset     = 1'b0;
        #10000 $finish;
    end


    always begin
        #15 IN = 1'b1;
        #100 IN = 1'b0;
    end





    always
          #5 clock = ~clock;


          initial begin
          $display("clk out");
          $dumpfile("MIDI_UART.vcd");
          $dumpvars(0,tb_MIDI_UART);
          //$dumpvars(0,tb_midi.U0.oscillator[0]);//schleife
          //$monitor("CLK:%b,reset:%b",clock,reset);
        end


endmodule
