module pwm (input wire clock,
            input wire reset,
            input wire ce_pwm,
            input wire [7:0] sample,
            output wire out);

    reg [7:0] pwm_cnt;
    reg pwm_out;
    reg [7:0] sample_reg;

    always @ (posedge clock or posedge reset)
    begin
        if (reset == 1'b1) begin
            pwm_cnt    <= 8'b0;
            pwm_out    <= 1'b0;
            sample_reg <= 8'b0;
        end else if (ce_pwm == 1'b1) begin
            pwm_cnt <= pwm_cnt + 1;
            if (pwm_cnt == 0)
                sample_reg <= sample;
            if (pwm_cnt >= sample)
                pwm_out <= 1'b0;
            else
                pwm_out <= 1'b1;
        end
    end

    assign out = pwm_out;

endmodule
