`timescale 1ns/100ps

module tb_voice ();

    reg clock;
    reg ce25;
    reg reset;
    wire [3:0]en_cnt;
    wire [11:0]sam_cnt;
    wire out_en;
    wire out_sam;
    wire out_pwm;
    reg [15:0] tone;
    reg [15:0] envelope;
    wire [15:0] tone_out;
    wire [15:0] mixer_out;
    wire [15:0] envelope_out;
    wire [15:0] voice_out;


    clken U0(clock,reset,ce25,en_cnt,sam_cnt,out_en,out_sam,out_pwm);

    tone_generator U1 (clock,reset, ce25,16'h000f,tone_out);

    voice  U2(clock, ce_25, ce_25, reset,16'h1111,16'h00ff,16'h00ff,16'hffff,16'h0000,1,
                    envelope_out,
                    out_en);

    envelope_generator U3(clock,reset,out_en,16'h00ff,16'h00ff,16'hffff,16'h0000,gate,1,envelope_out);

    mixer U4(clock,reset,ce25,tone,envelope,voice_out);


    initial begin
        clock     = 1'b0;
        ce25      = 1'b0;
        reset     = 1'b1;
        #10;
        reset     = 1'b0;
        tone = 16'h00ff;
        envelope = 16'h00ff;
        #200;
        tone = 16'h00ff;
        envelope = 16'hffff;
        #400;
        tone = 16'hffff;
        envelope = 16'hffff;
        #600;
        tone = 16'hffff;
        envelope = 16'h00ff;
        #10000 $finish;
    end





    always
          #5 clock = ~clock;

    always
          #20 ce25 = ~ce25;


          initial begin
          $display("clk out");
          $dumpfile("voice.vcd");
          $dumpvars(0,tb_voice);
          $monitor("CLK:%b,reset:%b,OUT:%b,EN_OUT:%b",clock,reset,voice_out ,out_en);
        end

        integer f;
        initial begin
        f = $fopen("out_rect.csv "); if(!f) $finish;
        end
        always @(posedge clock)
        $fdisplay(f, "%d",voice_out);

endmodule
