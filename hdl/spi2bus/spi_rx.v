module spi_rx (input clock, input reset, input mosi, input sck, input ss, output [23:0] rxdata, output reg rxen);

    reg enable;
    reg [2:0] sck_syncd;
    reg [2:0] ss_syncd;
    reg [4:0] data_cnt;
    reg [23:0] data;
    //reg [23:0] outputdata;

    assign rxdata = data;

    always @(posedge clock or posedge reset)
    begin
        if (reset) begin
            sck_syncd  <= 3'b0;
            ss_syncd   <= 3'b111;
        end else begin
            sck_syncd  <= {sck_syncd[1:0], sck};
            ss_syncd   <= {ss_syncd[1:0], ss};
        end
    end

    always @(posedge clock or posedge reset)
    begin
        if (reset) begin
            rxen <= 1'b0;
            enable <= 1'b0;
            data <= 24'b0;
            //outputdata <= 24'b0;
            data_cnt <= 5'b0;
        end else begin
            rxen <= 1'b0;
            if (ss_syncd[2:1] == 2'b1) begin
               data     <= 8'b0;
               data_cnt <= 5'b0;
            end else begin
                if (sck_syncd[2:1] == 2'b01) begin
                    data     <= {data[22:0],mosi};
                    data_cnt <= data_cnt + 1;
                    if (data_cnt == 5'b10111) begin
                        enable     <= 1'b1;
                    end

                    if(data_cnt == 5'b10111) begin
                        enable     <= 1'b0;
                        //outputdata <= data;
                        rxen       <= 1'b1;
                        data_cnt   <= 5'b0;
                    end
                end
            end
        end
    end

endmodule
