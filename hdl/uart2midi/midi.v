`default_nettype none
module midi (input wire clock,
            input wire reset,
            input wire [7:0] rx_data,
            input wire new_rx_data,
            output wire [7:0] gate,
            output wire [15:0] freq0,
            output wire [15:0] freq1,
            output wire [15:0] freq2,
            output wire [15:0] freq3,
            output wire [15:0] freq4,
            output wire [15:0] freq5,
            output wire [15:0] freq6,
            output wire [15:0] freq7);


parameter NOTE_ON=8'h90,
          NOTE_OFF=8'h80,
          SYSTEM=8'hF0,
          EXPECT_COMMAND=2'b00,
          EXPECT_NOTE=2'b01,
          EXPECT_VELOCITY= 2'b10,
          OSZILLATOR_CHANNEL= 8,
          END = 8'b11111111,
          WAIT = 8'b00000000,
          OSCI_0 = 8'b00000001,
          OSCI_1 = 8'b00000010,
          OSCI_2 = 8'b00000100,
          OSCI_3 = 8'b00001000,
          OSCI_4 = 8'b00010000,
          OSCI_5 = 8'b00100000,
          OSCI_6 = 8'b01000000,
          OSCI_7 = 8'b10000000;



          reg test;
          reg [7:0] int_gate;
          reg [7:0] int_OUTgate;
          reg [15:0]  freqtable [127:0];
          reg [7:0] oscillator [7:0];
          reg [15:0] freq [7:0];
          reg wr_en;
          reg clr_en;
          reg start;
          reg [1:0]statems;
          reg [7:0]state;
          reg [7:0]oscillator_state;
          reg [7:0] int_IN;
          reg full;
          reg [7:0]s_note;
          integer i;
          integer j;
          integer address;
          reg int_note_on;
          reg int_note_off;




    always @ (posedge clock or posedge reset)
    begin
        if (reset == 1'b1) begin
        statems <= EXPECT_COMMAND;
		    start <= 1'b0;
        state <= 8'h00;
        wr_en <= 1'b0;
        clr_en <= 1'b0;
        int_IN <= 8'h00;
        int_OUTgate <= 8'h00;
        address <= 0;
        int_note_on <= 1'b0;
        int_note_off <= 1'b0;
		    i=0;
        j=0;
        test <= 1'b0;
        full<=1'b0;
        oscillator_state<=WAIT;
        int_gate <= 8'h00;
        freq[0]= 16'h00;
        freq[1]= 16'h00;
        freq[2]= 16'h00;
        freq[3]= 16'h00;
        freq[4]= 16'h00;
        freq[5]= 16'h00;
        freq[6]= 16'h00;
        freq[7]= 16'h00;
        oscillator[0]= 8'h00;
        oscillator[1]= 8'h00;
        oscillator[2]= 8'h00;
        oscillator[3]= 8'h00;
        oscillator[4]= 8'h00;
        oscillator[5]= 8'h00;
        oscillator[6]= 8'h00;
        oscillator[7]= 8'h00;
        end
        else
          begin
            if(/*IN != int_IN*/new_rx_data == 1'b1) begin
              int_IN <= rx_data;
              state = rx_data & 8'hF0;
              case (statems)
                EXPECT_COMMAND:begin
                  case(state)
                    NOTE_ON:
                    begin
                      statems <= EXPECT_NOTE;
                      int_note_on <= 1'b1;
                    end
                    NOTE_OFF:
                    begin
                      statems <= EXPECT_NOTE;
                      int_note_off <= 1'b1;
                    end
                    //SYSTEM: statems <= 2'b00;


                  endcase
                  end

                EXPECT_NOTE:begin
                if(rx_data == 8'hFE) begin

                  //int_OUTgate <= int_gate;

                  statems <= EXPECT_COMMAND;
                  //int_note_on <= 1'b0;
                  //int_note_off <= 1'b0;
                end
                else
                begin
				          //start <= 1'b1;

                  s_note <= rx_data;
                  statems <= EXPECT_VELOCITY;

                end
                end
                EXPECT_VELOCITY:begin
                  address <= s_note;
                  if (/*state == NOTE_ON*/ int_note_on == 1'b1 && rx_data > 8'd0 && oscillator_state == WAIT && full != 1'b1 && s_note !=       oscillator[0] && s_note != oscillator[1] && s_note != oscillator[2] && s_note != oscillator[3] && s_note != oscillator[4] && s_note != oscillator[5] && s_note != oscillator[6] && s_note != oscillator[7]) begin
                    if(wr_en != 1'b1) begin
                      wr_en <= 1'b1;
                    end
                    oscillator_state<= OSCI_0;
                    //int_note_on <= 1'b0;
                  end
                  else if (((full != 1 && rx_data == 8'h00)||(full == 1'b1 && rx_data == 8'h00)) && (s_note == oscillator[0] || s_note == oscillator[1] || s_note == oscillator[2] || s_note == oscillator[3] || s_note == oscillator[4] || s_note == oscillator[5] || s_note == oscillator[6] || s_note == oscillator[7])) begin
                   clr_en <= 1'b1;
                   oscillator_state<= OSCI_0;
                   int_note_on <= 1'b1;
                   test <= 1'b0;
                   /*end
                   else if  begin
                   clr_en <= 1'b1;
                   oscillator_state<= OSCI_0;
                   int_note_on <= 1'b1;
                   test <= 1'b0;*/
                   end
                   else
                   begin
                     statems <= EXPECT_NOTE;
                     test <= 1'b1;
                   end
                  end
                endcase
              end
            case (oscillator_state )
              OSCI_0:
              begin
				if(wr_en == 1'b1) begin
					i = 0;
		            if(oscillator[i] == 8'h00) begin
		              int_gate[i] <= 1'b1;
		              oscillator[i] =address;
		              freq[i] <= freqtable[address];
		              wr_en <= 1'b0;
		              statems <= EXPECT_NOTE;
		              oscillator_state <= WAIT;
		        end else begin
                  oscillator_state <= OSCI_1;
               end
				end else if(clr_en == 1'b1) begin
					j = 0;
		            if(oscillator[j] == address) begin
		              int_gate[j] <= 1'b0;
		              oscillator[j] =8'h00;
		              freq[j] = 16'h00;
                  clr_en <= 1'b0;
                  statems <= EXPECT_NOTE;
		              full <= 1'b0;
		              oscillator_state <= WAIT;
		           end else begin
					oscillator_state <= OSCI_1;
				   end
				end else begin
					  oscillator_state <= WAIT;
				end
              end
              OSCI_1: begin
				if(wr_en == 1'b1) begin
					i = 1;
		            if(oscillator[i] == 8'h00) begin
		              int_gate[i] <= 1'b1;
		              oscillator[i] =s_note;
		              freq[i] <= freqtable[address];
		              wr_en <= 1'b0;
		              statems <= EXPECT_NOTE;
		              oscillator_state <= WAIT;
		        end else begin
                  oscillator_state <= OSCI_2;
               end
				end else if(clr_en == 1'b1) begin
					j = 1;
		            if(oscillator[j] == address) begin
		              int_gate[j] <= 1'b0;
		              oscillator[j] =8'h00;
		              freq[j] = 16'h00;
                  clr_en <= 1'b0;
                  statems <= EXPECT_NOTE;
		              full <= 1'b0;
		              oscillator_state <= WAIT;
		           end else begin
					oscillator_state <= OSCI_2;
				   end
				end else begin
					  oscillator_state <= WAIT;
				end

              end
              OSCI_2: begin
				if(wr_en == 1'b1) begin
					i = 2;
		            if(oscillator[i] == 8'h00) begin
		              int_gate[i] <= 1'b1;
		              oscillator[i] =address;
		              freq[i] <= freqtable[address];
		              wr_en <= 1'b0;
		              statems <= EXPECT_NOTE;
		              oscillator_state <= WAIT;
		        end else  begin
                  oscillator_state <= OSCI_3;
               end
				end else if(clr_en == 1'b1) begin
					j = 2;
		            if(oscillator[j] == address) begin
		              int_gate[j] <= 1'b0;
		              oscillator[j] =8'h00;
		              freq[j] = 16'h00;
                  clr_en <= 1'b0;
                  statems <= EXPECT_NOTE;
		              full <= 1'b0;
		              oscillator_state <= WAIT;
		           end else begin
					oscillator_state <= OSCI_3;
				   end
				end else begin
					  oscillator_state <= WAIT;
				end
              end
			  OSCI_3: begin
				if(wr_en == 1'b1) begin
					i = 3;
		            if(oscillator[i] == 8'h00) begin
		              int_gate[i] <= 1'b1;
		              oscillator[i] =address;
		              freq[i] <= freqtable[address];
		              wr_en <= 1'b0;
		              statems <= EXPECT_NOTE;
		              oscillator_state <= WAIT;
		        end else  begin
                  oscillator_state <= OSCI_4;
               end
				end else if(clr_en == 1'b1) begin
					j = 3;
		            if(oscillator[j] == address) begin
		              int_gate[j] <= 1'b0;
		              oscillator[j] =8'h00;
		              freq[j] = 16'h00;
                  statems <= EXPECT_NOTE;
                  clr_en <= 1'b0;
		              full <= 1'b0;
		              oscillator_state <= WAIT;
		           end else begin
					oscillator_state <= OSCI_4;
				   end
				end else begin
					  oscillator_state <= WAIT;
				end
              end
              OSCI_4: begin
				if(wr_en == 1'b1) begin
					i = 4;
		            if(oscillator[i] == 8'h00) begin
		              int_gate[i] <= 1'b1;
		              oscillator[i] =address;
		              freq[i] <= freqtable[address];
		              wr_en <= 1'b0;
		              statems <= EXPECT_NOTE;
		              oscillator_state <= WAIT;
		        end else  begin
                  oscillator_state <= OSCI_5;
               end
				end else if(clr_en == 1'b1) begin
					j = 4;
		            if(oscillator[j] == address) begin
		              int_gate[j] <= 1'b0;
		              oscillator[j] =8'h00;
		              freq[j] = 16'h00;
                  statems <= EXPECT_NOTE;
                  clr_en <= 1'b0;
		              full <= 1'b0;
		              oscillator_state <= WAIT;
		           end else begin
					oscillator_state <= OSCI_5;
				   end
				end else begin
					  oscillator_state <= WAIT;
				end
              end
              OSCI_5: begin
        if(wr_en == 1'b1) begin
          i = 5;
                if(oscillator[i] == 8'h00) begin
                  int_gate[i] <= 1'b1;
                  oscillator[i] =address;
                  freq[i] <= freqtable[address];
                  wr_en <= 1'b0;
                  statems <= EXPECT_NOTE;
                  oscillator_state <= WAIT;
            end else  begin
                  oscillator_state <= OSCI_6;
               end
        end else if(clr_en == 1'b1) begin
          j = 5;
                if(oscillator[j] == address) begin
                  int_gate[j] <= 1'b0;
                  oscillator[j] =8'h00;
                  freq[j] = 16'h00;
                  clr_en <= 1'b0;
                  statems <= EXPECT_NOTE;
                  full <= 1'b0;
                  oscillator_state <= WAIT;
               end else begin
          oscillator_state <= OSCI_6;
           end
        end else begin
            oscillator_state <= WAIT;
        end
              end
        OSCI_6: begin
				if(wr_en == 1'b1) begin
					i = 6;
		            if(oscillator[i] == 8'h00) begin
		              int_gate[i] <= 1'b1;
		              oscillator[i] =address;
		              freq[i] <= freqtable[address];
		              wr_en <= 1'b0;
		              statems <= EXPECT_NOTE;
		              oscillator_state <= WAIT;
		        end else begin
                  oscillator_state <= OSCI_7;
               end
				end else if(clr_en == 1'b1) begin
					j = 6;
		            if(oscillator[j] == address) begin
		              int_gate[j] <= 1'b0;
		              oscillator[j] =8'h00;
		              freq[j] = 16'h00;
                  clr_en <= 1'b0;
                  statems <= EXPECT_NOTE;
		              full <= 1'b0;
		              oscillator_state <= WAIT;
		           end else begin
					oscillator_state <= OSCI_7;
				   end
				end else begin
					  oscillator_state <= WAIT;
				end
              end
              OSCI_7: begin
				if(wr_en == 1'b1) begin
					i = 7;
		            if(oscillator[i] == 8'h00) begin
		              int_gate[i] <= 1'b1;
		              oscillator[i] =address;
		              freq[i] <= freqtable[address];
		              wr_en <= 1'b0;
		              statems <= EXPECT_NOTE;
		              oscillator_state <= WAIT;
                  full <= 1'b1;
		        end else begin
                  oscillator_state <= WAIT;
				  wr_en <= 1'b0;
               end
				end else if(clr_en == 1'b1) begin
					j = 7  ;
		            if(oscillator[j] == address) begin
		              int_gate[j] <= 1'b0;
		              oscillator[j] =8'h00;
		              freq[j] = 16'h00;
                  clr_en <= 1'b0;
                  statems <= EXPECT_NOTE;
		              full <= 1'b0;
		              oscillator_state <= WAIT;
		           end else begin
					oscillator_state <= WAIT;
					clr_en <= 1'b0;
				   end
				end else begin
					  oscillator_state <= WAIT;
				end
              end
              WAIT: begin
              end

            endcase
end
end



    initial begin
       $readmemh("midi.mem",freqtable);
    end



  assign freq0 = freq[0];
  assign freq1 = freq[1];
  assign freq2 = freq[2];
  assign freq3 = freq[3];
  assign freq4 = freq[4];
  assign freq5 = freq[5];
  assign freq6 = freq[6];
  assign freq7 = freq[7];

  assign gate = int_gate;

  //assign freqOUT = freq;


endmodule //
