`timescale 1ns/1ps

module tb_midi ();

    reg clock;
    reg reset;
    reg [7:0]rx_data;
    reg new_rx_data;
    wire [15:0] freq0;
    wire [15:0] freq1;
    wire [15:0] freq2;
    wire [15:0] freq3;
    wire [15:0] freq4;
    wire [15:0] freq5;
    wire [15:0] freq6;
    wire [15:0] freq7;
    wire [7:0] gate;
    //reg [15:0] freq [7:0];

    midi U0(clock,reset,rx_data,new_rx_data,gate,freq0,freq1,freq2,freq3,freq4,freq5,freq6,freq7);

    initial begin
        clock     = 1'b0;
        reset     = 1'b1;
        rx_data        = 8'b0;
        new_rx_data    = 1'b0;
        #10;
        reset     = 1'b0;
        #10;
        rx_data        = 8'h90;
        #100;
        rx_data        = 8'h6c; //#1
        #100;
        rx_data        = 8'h60;
        #100;
        rx_data        = 8'h6b; //#2
        #100;
        rx_data        = 8'h60;
        #100;
        rx_data        = 8'h6a; //#3
        #100;
        rx_data        = 8'h60;
        #100;
        rx_data        = 8'h1d; //#5
        #100;
        rx_data        = 8'h60;
        #100;
        rx_data        = 8'h18; //#6
        #100;
        rx_data        = 8'h60;
        #100;
        rx_data        = 8'h19; //#7
        #100;
        rx_data        = 8'h60;
        #100;
        rx_data        = 8'h1A; //#8
        #100;
        rx_data        = 8'h60;
        #100;
        rx_data        = 8'h1b; //#9
        #100;
        rx_data        = 8'h60;
        #100;
        rx_data        = 8'h1e; //#10
        #100;
        rx_data        = 8'h60;
        #100;
        rx_data        = 8'h1f; //#11
        #100;
        rx_data        = 8'h60;
        #100;
        rx_data        = 8'h2f; //#11
        #100;
        rx_data        = 8'h60;
        #100;
        rx_data        = 8'hFE;
        #100;
        rx_data        = 8'h90;
        #100;
        rx_data        = 8'h1a; //#-1
        #100;
        rx_data        = 8'h00;
        #100;
        rx_data        = 8'h6c; //#-1
        #100;
        rx_data        = 8'h00;
        #100;
        rx_data        = 8'h6b; //#-2
        #100;
        rx_data        = 8'h60;
        #100;
        rx_data        = 8'h6a;
        #100;
        rx_data        = 8'h00;
        #100;
        #10000 $finish;
    end

    always begin
        #90 new_rx_data = 1'b1;
        #10 new_rx_data = 1'b0;
    end





    always
          #5 clock = ~clock;
          initial begin
          $display("clk out");
          $dumpfile("midi.vcd");
          $dumpvars(0,tb_midi);
          //$dumpvars(0,tb_midi.U0.oscillator[0]);//schleife
          //$monitor("CLK:%b,reset:%b",clock,reset);
        end


endmodule
