module spi2bus_top(input clock, input reset, input ss, input mosi, input sck,
                   output  [15:0] int_address,  // address bus to register file
                   output  [7:0]  int_wr_data,  // write data to register file
                   output         int_write);    // write control to register file


wire [23:0] rx_data;		// data byte received
wire       new_rx_data;	// signs that a new byte was received

spi_rx spi_rx1 (.clock(clock), .reset(reset), .mosi(mosi), .sck(sck), .ss(ss), .rxdata(rx_data), .rxen(new_rx_data));


// uart parser instance
spi_parser #(16) spi_parser1
(
	.clock(clock), .reset(reset),
	.rx_data(rx_data), .new_rx_data(new_rx_data),
	.int_address(int_address), .int_wr_data(int_wr_data), .int_write(int_write),
	.int_req(), .int_gnt(1)
);

endmodule
