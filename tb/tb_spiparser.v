`timescale 1ns/100ns

module tb_spiparser ();

    reg clock;
    reg reset;
    reg [23:0] rxdata;
    reg rxen;
    wire [15:0]o_address;
    wire [7:0]o_data;
    wire o_write;



    spi_parser U0(clock,reset,rxdata,rxen,o_address,o_data,o_write,1'b1,1'b1);

    initial begin
        clock     = 1'b0;
        reset     = 1'b1;
        rxdata = 24'h000000;
        rxen = 1'b0;
        #10;
        reset     = 1'b0;
        rxen = 1'b1;
        #10
        rxdata = 24'h800000;
        #10
        rxdata = 24'h452000;
        #100000000 $finish;
    end




    always
          #5 clock = ~clock;



          initial begin
          $display("spi_parser out");
          $dumpfile("spi_parser.vcd");
          $dumpvars(0,tb_spiparser);
        //  $monitor("clk:%d,sck:%d,mosi :%d, data:%d",clock, sck,mosi,rxdata);
        end

endmodule
