		//---------------------------------------------------------------------------------------
// uart parser module
//
//---------------------------------------------------------------------------------------

module spi_parser
(
	// global signals
	clock, reset,
	// transmit and receive internal interface signals from uart interface
	rx_data, new_rx_data,
	// internal bus to register file
	int_address, int_wr_data, int_write,
	int_req, int_gnt
);
//---------------------------------------------------------------------------------------
// parameters
parameter		AW = 16;			// address bus width parameter

// modules inputs and outputs
input 			clock;			// global clock input
input 			reset;			// global reset input



input	[23:0]	rx_data;		// data byte received
input 			new_rx_data;	// signs that a new byte was received
output	[AW-1:0] int_address;	// address bus to register file
output	[7:0]	int_wr_data;	// write data to register file
output			int_write;		// write control to register file
output			int_req;		// bus access request signal
input			int_gnt;		// bus access grant signal

// registered outputs
reg	[AW-1:0] int_address;
reg	[7:0] int_wr_data;
reg  int_write;
reg write_req;



// binary mode extension states
`define MAIN_IDLE		3'b000
`define MAIN_BIN_CMD	3'b100
`define MAIN_BIN_DATA_ADDR	3'b010



// binary extension mode commands - the command is indicated by bits 5:4 of the command byte
//`define BIN_CMD_NOP		2'b00
//`define BIN_CMD_WRITE	2'b10

// internal wires and registers
reg [3:0] main_sm;			// main state machine
//reg write_op;				// write operation flag
//reg bin_write_op;			// binary mode write operation flag
//reg send_stat_flag;			// send status flag
reg int_data_sent;

/*//---------------------------------------------------------------------------------------
// module implementation
// main state machine
always @ (posedge clock or posedge reset)
begin
	if (reset)
		main_sm <= `MAIN_IDLE;
	else if (new_rx_data)
	begin
		case (main_sm)
			// wait for a read ('r') or write ('w') command
			// binary extension - an all zeros byte enabled binary commands
			`MAIN_IDLE:
				// check received character
				if (rx_data == 24'h0)
					// an all zeros received byte enters binary mode
					main_sm <= `MAIN_BIN_DATA_ADDR;


	/*		// binary extension
			// wait for command - one byte
			`MAIN_BIN_CMD:
				// check if command is a NOP command
				if (rx_data[23:22] == `BIN_CMD_NOP)
					// if NOP command then switch back to idle state
					main_sm <= `MAIN_IDLE;
				else
					// not a NOP command, continue receiving parameters
					main_sm <= `MAIN_BIN_DATA_ADDR;



			// on write commands wait for data till end of buffer as specified by length parameter
			`MAIN_BIN_DATA_ADDR:
				if(int_data_sent == 1'b1) begin
					main_sm <= `MAIN_IDLE;
					int_data_sent <= 1'b0;
					end

			// go to idle
			default:
				main_sm <= `MAIN_IDLE;
		endcase
	end
end

/*

// binary mode write operation flag
always @ (posedge clock or posedge reset)
begin
	if (reset)
		bin_write_op <= 1'b0;
	else if ((main_sm == `MAIN_BIN_CMD) && new_rx_data && (rx_data[23:22]  == `BIN_CMD_WRITE))
		// write command is started on reception of a write command
		bin_write_op <= 1'b1;
	else if ((main_sm == `MAIN_BIN_DATA_ADDR) && new_rx_data && int_data_sent)
		bin_write_op <= 1'b0;
end


*/

// internal write control and data
always @ (posedge clock or posedge reset)
begin
	if (reset)
	begin
		write_req <= 1'b0;
		int_write <= 1'b0;
		int_wr_data <= 8'h00;
		int_data_sent <= 1'b0;
	end
	// binary extension mode
	else if (new_rx_data)
	begin
		write_req <= 1'b1;
		int_wr_data <= rx_data[7:0];
		int_address <= rx_data[23:8];
	end
	else if (int_gnt && write_req)
	begin
		// set internal bus write and clear the write request flag
		int_write <= 1'b1;
		int_data_sent <= 1'b1;
		write_req <= 1'b0;
	end
	else
		int_write <= 1'b0;
end

//assign address = int_address;
//assign wr_data = int_wr_data;
//assign write = int_write;

//Porgramm vereinfachen





endmodule
//---------------------------------------------------------------------------------------
//						Th.. Th.. Th.. Thats all folks !!!
//---------------------------------------------------------------------------------------
