#!/usr/bin/env python2
import mido
import serial
import sys
import time
import signal
import spidev
import argparse


def set_gate(port, voice):
    write(port,0x100+voice,13)



def set_adsr(port,voice,a,d,s,r):
	write(port,0x10*(voice)+2,a%256)
	write(port,0x10*(voice)+3,a/256)
	write(port,0x10*(voice)+4,d%256)
	write(port,0x10*(voice)+5,d/256)
	write(port,0x10*(voice)+6,s%256)
	write(port,0x10*(voice)+7,s/256)
	write(port,0x10*(voice)+8,r%256)
	write(port,0x10*(voice)+9,r/256)



def write(port,address,byte):

    if address > 65535:
        raise ValueError("Out of address space")

    data = [address/256,address%256,byte]

    print data

    port.xfer2(data,32000,1000,8)

    time.sleep(0.01)

    return True

if __name__=="__main__":

    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('--usynth_param',dest  = 'N', type=int,required=False, nargs=4,action='append',help='Attack Decay Sustain Release')

    args = parser.parse_args()

    usynth_param = args.N
    midi_gates = len(args.N)

    Param_Values = 0

    for list in args.N:

        for x in range(0, 4):
            if not 0 < list[x] < 2**16:
                sys.stderr.write(str(x+1    ) + " parameter of the "+ str(Param_Values) + " channel phrase is out of range!!! (" + str(list[x]) + ")")
                sys.exit(-1)
        Param_Values = Param_Values +1



    try:
        port = spidev.SpiDev()
        port.open(0,1)
    except Exception as e:
        port.close()

        sys.stderr.write("Port cannot be opened: ")#%s\n", str(e)")
        sys.exit(-1)

    v = 0
    for list in args.N:
        set_adsr(port,v,list[0],list[1],list[2],list[3])
        set_gate(port,v)
        v = v + 1



    port.close()
