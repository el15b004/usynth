`timescale 1ns/100ps

module tb_clken ();

    reg clock;
    reg ce25;
    reg reset;
    wire [3:0]en_cnt;
    wire [11:0]sam_cnt;
    wire out_en;
    wire out_sam;
    wire out_pwm;


    clken U0(clock,reset,ce25,en_cnt,sam_cnt,out_en,out_sam,out_pwm);
    initial begin
        clock     = 1'b0;
        ce25      = 1'b0;
        reset     = 1'b1;
        #10;
        reset     = 1'b0;
        #100000000 $finish;
    end





    always
          #5 clock = ~clock;

    always
          #20 ce25 = ~ce25;


          initial begin
          $display("clk out");
          $dumpfile("clk.vcd");
          $dumpvars(0,tb_clken);
          $monitor("clk:%b,r:%b,envelope_count:%d,sample_count:%d,env_out:%b,sample_out:%b,pwm_out:%b",clock, ce25,en_cnt,sam_cnt, out_en,out_sam,out_pwm);
        end

endmodule
