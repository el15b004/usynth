`timescale 1ns/100ps

module tb_spi ();

    reg clock;
    reg reset;
    reg mosi;
    reg sck;
    reg ss;
    wire [23:0] rxdata;
    wire rxen;



    spi_rx U0(clock,reset,mosi,sck,ss,rxdata,rxen);
    initial begin
        clock     = 1'b0;
        reset     = 1'b1;
        ss        = 1'b0;
        sck       = 1'b0;
        mosi      = 1'b0;
        #10;
        reset     = 1'b0;
        #100000000 $finish;
    end


    always
          #400 mosi = ~mosi;

    always
          #100 sck = ~sck;

    always
          #5 clock = ~clock;



          initial begin
          $display("clk out");
          $dumpfile("spi.vcd");
          $dumpvars(0,tb_spi);
          $monitor("clk:%d,sck:%d,mosi :%d, data:%d",clock, sck,mosi,rxdata);
        end

endmodule
