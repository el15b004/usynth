module mixer  (input wire clock, input wire reset,
               input wire ce, input wire [15:0] tone,
               input wire [15:0] envelope, output wire [15:0] out);

   reg   [15:0]  sample;
   wire  [15:0] inv;
   wire  [31:0] scale;
   wire  [15:0] sample_next;

   assign inv         = (tone[15:8] != 8'b0) ? (tone[15:0] - 16'h8000) : (16'h8000 - tone[15:0]);
   assign scale       = {16'b0, inv} * {16'b0, envelope[15:0]};
   assign sample_next = (tone[15:8] != 8'b0) ? (scale[31:16] + 16'h8000) : (16'h8000 - scale[31:16]);

   always @ (posedge clock or posedge reset)
   begin
      if (reset)
         sample <= 16'b0;
      else if (ce) begin
         sample <= sample_next;
         //$display("%d", sample[31:16]);
      end
   end

   assign out = sample;

endmodule
