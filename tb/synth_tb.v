`timescale 1ns/100ps

module tb_synth ();

    reg play_note;
    reg clock;
    reg ce25;
    reg reset;
    wire out;
    

    synth dut (.clock(clock), .ce25(ce25), .reset(reset),
               .play_note(play_note), .out(out));

    initial begin
        clock     = 1'b0;
        ce25      = 1'b0;
        play_note = 1'b0;
        reset     = 1'b1;
        #15;
        reset     = 1'b0;
        #35;
        play_note = 1'b1;
        #(976380*64)
        play_note = 1'b0;
        #(976380*64)
        $finish;
    end

    always
        #5 clock = ~clock;

    always
        #20 ce25 = ~ce25;

endmodule
