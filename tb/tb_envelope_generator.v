`timescale 1ns/100ps

module tb_envelope_generator ();

    reg clock;
    reg ce25;
    reg reset;
    wire [3:0]en_cnt;
    wire [11:0]sam_cnt;
    wire out_en;
    wire out_sam;
    wire out_pwm;
    wire [15:0] out;
    reg gate;
    reg enable;


    clken U0(clock,reset,ce25,en_cnt,sam_cnt,out_en,out_sam,out_pwm);


    envelope_generator U1(clock,reset,out_en,16'h00ff,16'h00ff,16'hffff,16'h0000,gate,1,out);
    initial begin
        clock     = 1'b0;
        ce25      = 1'b0;
        reset     = 1'b1;
        gate      = 1'b0;
        #10;
        reset     = 1'b0;
        #35;
        gate = 1'b1;
        #(976380*64)
        gate = 1'b0;
        #(976380*64)
        #10000 $finish;
    end


    always
          #100 enable = ~enable;

    always
          #1000 gate= ~gate;


    always
          #5 clock = ~clock;

    always
          #20 ce25 = ~ce25;


          initial begin
          $display("en out");
          $dumpfile("en.vcd");
          $dumpvars(0,tb_envelope_generator);
          $monitor("clk:%b,ce25:%b,out:%d",clock, out_en,out);
        end

        integer f;
        initial begin
        f = $fopen("out_envelope_gen.csv "); if(!f) $finish;
        end
        always @(posedge clock)
        $fdisplay(f, "%d",out);

endmodule
