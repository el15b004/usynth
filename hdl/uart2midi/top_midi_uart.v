//---------------------------------------------------------------------------------------
// uart to internal bus top module
//
//---------------------------------------------------------------------------------------

module top_midi_uart
(
	// global signals
	clock, reset,
	// uart serial signals
	MIDILED,
	ser_in,
  gate,
  freq,
	ser_out
);
//---------------------------------------------------------------------------------------
// modules inputs and outputs
input 			clock;			// global clock input
input 			reset;			// global reset input
input			  ser_in;			// serial data input
output      [7:0]gate;
output      [8*16-1:0]freq;
output 			[7:0]MIDILED;
output 			ser_out;



// baud rate configuration, see baud_gen.v for more details.
// baud rate generator parameters for 460800 baud on 24MHz clock
`define D_BAUD_FREQ			12'd1
`define D_BAUD_LIMIT		16'd48
// baud rate generator parameters for 115200 baud on 44MHz clock
 `define D_BAUD_FREQ2			12'd24
 `define D_BAUD_LIMIT2		16'd601
// baud rate generator parameters for 9600 baud on 66MHz clock
//`define D_BAUD_FREQ		12'h10
//`define D_BAUD_LIMIT		16'h1ACB


// internal wires
wire	[7:0]	tx_data;		// data byte to transmit
wire			new_tx_data;	// asserted to indicate that there is a new data byte for transmission
wire 			tx_busy;		// signs that transmitter is busy
wire	[7:0]	rx_data;		// data byte received
wire 			new_rx_data;	// signs that a new byte was received
wire	[11:0]	baud_freq;
wire	[15:0]	baud_limit;
wire			baud_clk;


wire	[7:0]	rx_data2;		// data byte received
wire 			new_rx_data2;	// signs that a new byte was received
wire	[11:0]	baud_freq2;
wire	[15:0]	baud_limit2;
wire			baud_clk2;


assign MIDILED = rx_data2;

//---------------------------------------------------------------------------------------
// module implementation
// uart top module instance
uart_top uart1
(
	.clock(clock), .reset(reset),
	.ser_in(ser_in), //.ser_out(ser_out),
	.rx_data(rx_data), .new_rx_data(new_rx_data),
	//.tx_data(tx_data), .new_tx_data(new_tx_data), .tx_busy(tx_busy),
	.baud_freq(baud_freq), .baud_limit(baud_limit),
	.baud_clk(baud_clk)
);

/*uart_top uart2
(
	.clock(clock), .reset(reset),
	.ser_in(ser_in), .ser_out(ser_out),
	.rx_data(rx_data2), .new_rx_data(new_rx_data2),
	//.tx_data(tx_data), .new_tx_data(new_tx_data), .tx_busy(tx_busy),
	.baud_freq(baud_freq2), .baud_limit(baud_limit2),
	.baud_clk(baud_clk2)
);*/

//assign out_rx = rx_data2;

// assign baud rate default values
assign baud_freq = `D_BAUD_FREQ;
assign baud_limit = `D_BAUD_LIMIT;

assign baud_freq2 = `D_BAUD_FREQ2;
assign baud_limit2 = `D_BAUD_LIMIT2;

wire [15:0]freq0;
wire [15:0]freq1;
wire [15:0]freq2;
wire [15:0]freq3;
wire [15:0]freq4;
wire [15:0]freq5;
wire [15:0]freq6;
wire [15:0]freq7;

midi midi_1
(
  .clock(clock), .reset(reset),
  .rx_data(rx_data), .new_rx_data(new_rx_data),
  .gate(gate),
  .freq0(freq0),.freq1(freq1),
  .freq2(freq2),.freq3(freq3),
  .freq4(freq4),.freq5(freq5),
  .freq6(freq6),.freq7(freq7)
  );

	assign freq[15:0] = freq0;
	assign freq[31:16] = freq1;
	assign freq[47:32] = freq2;
	assign freq[63:48] = freq3;
	assign freq[79:64] = freq4;
	assign freq[95:80] = freq5;
	assign freq[111:96] = freq6;
	assign freq[127:112] = freq7;

endmodule
//---------------------------------------------------------------------------------------
//						Th.. Th.. Th.. Thats all folks !!!
//---------------------------------------------------------------------------------------
