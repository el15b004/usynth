`timescale 1ns/100ps

module tb_mixer ();

    reg clock;
    reg ce25;
    reg reset;
    wire [3:0]en_cnt;
    wire [11:0]sam_cnt;
    wire out_en;
    wire out_sam;
    wire out_pwm;
    reg [15:0] tone;
    reg [15:0] envelope;
    wire [15:0] out;

    clken U1(clock,reset,ce25,en_cnt,sam_cnt,out_en,out_sam,out_pwm);

    mixer U0(clock,reset,ce25,tone,envelope,out);

    initial begin
        clock     = 1'b0;
        ce25      = 1'b0;
        reset     = 1'b1;
        #10;
        reset     = 1'b0;
        tone = 16'h00ff;
        envelope = 16'h00ff;
        #200;
        tone = 16'h00ff;
        envelope = 16'hffff;
        #400;
        tone = 16'hffff;
        envelope = 16'hffff;
        #600;
        tone = 16'hffff;
        envelope = 16'h00ff;
        #10000 $finish;
    end



    always
          #5 clock = ~clock;

    always
          #20 ce25 = ~ce25;


          initial begin
          $display("clk out");
          $dumpfile("mixer.vcd");
          $dumpvars(0,tb_mixer);
          $monitor("CLK:%b,reset:%b,OUT:%b",clock,reset,out);
        end


        integer f;
        initial begin
        f = $fopen("out_mixer.csv "); if(!f) $finish;
        end
        always @(posedge clock)
        $fdisplay(f, "%d",out);

endmodule
