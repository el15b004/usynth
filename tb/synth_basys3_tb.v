`timescale 1ns/100ps

module synth_basys3_tb ();

    reg clock100;
    reg reset;
    reg key;
    wire pwm;
    reg [15:0] sw;

    synth_basys3 dut (.clock100(clock100), .reset(reset),
                      .key(key), .pwm(pwm), .sw(sw));
                  
    initial begin
        clock100 = 1'b0;
        reset = 1'b1;
        key   = 1'b0;
        sw    = 16'h01;
        #5;
        reset = 1'b0;
        #100;
        key   = 1'b1;
        #20000000;
        #20000000;
        key   = 1'b0;
        #20000000;
        #20000000;
    end
    
    always
        #5 clock100 = ~clock100;


endmodule
