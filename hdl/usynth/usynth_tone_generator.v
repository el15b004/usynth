module tone_generator #(parameter WAVEFORM = 0)
                         (input wire clock,
                         input wire reset,
                         input wire ce,
                         input wire [15:0] freq,
                         output wire [15:0] out);

   reg [12:0] phacc;
   reg [7:0]  wavetable [4095:0];
   wire [15:0] wave_out;
   wire [11:0] wavetable_addr;
   reg [7:0] wavetable_out;


   always @(posedge clock or posedge reset)
   begin
      if (reset) begin
         phacc <= 13'b0;
      end else if (ce) begin
         phacc <= phacc + {1'b0, freq[11:0]}; // Don't allow increments > 1/2
      end
   end

   initial begin
      $readmemh("wavetable.mem",wavetable);
   end

   assign wavetable_addr = phacc[11:0];

generate
   if (WAVEFORM == 0) /* Square */
      assign wave_out[15:8] = (phacc[12]) ? 8'hff : 8'h00;
   else if (WAVEFORM == 1) /* SINE */
      assign wave_out[15:8] = (phacc[12]) ? 8'hff-wavetable_out : wavetable_out;
endgenerate

   assign wave_out[7:0]  = 8'b0;
   assign out = wave_out;

   always @(posedge clock)
      wavetable_out <= wavetable[wavetable_addr];


endmodule
