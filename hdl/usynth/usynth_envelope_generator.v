module envelope_generator  (input wire clock,
                            input wire reset,
                            input wire ce,
                            input wire [15:0] a,    // attack
                            input wire [15:0] d,    // decay
                            input wire [15:0] s,    // sustain
                            input wire [15:0] r,    // release
                            input wire gate,
                            input wire enable,
                            output wire [15:0] out);

    reg [15:0] acc;
    reg [15:0] a_sampled;
    reg [15:0] d_sampled;
    reg [15:0] s_sampled;
    reg [15:0] r_sampled;
    reg last_gate;
    reg [4:0] adsr_state;

    wire posedge_gate;

    assign posedge_gate = gate & ~last_gate;

    localparam  IDLE    = 5'b00001,
                ATTACK  = 5'b00010,
                DECAY   = 5'b00100,
                SUSTAIN = 5'b01000,
                RELEASE = 5'b10000;

    always @ (posedge(clock) or posedge(reset))
    begin
        if (reset) begin
            a_sampled        <= 16'b0;
            d_sampled        <= 16'b0;
            s_sampled        <= 16'b0;
            r_sampled        <= 16'b0;
            acc              <= 16'b0;
            adsr_state       <= IDLE;
            last_gate        <= 1'b0;
        end else begin
            last_gate <= gate;
            if (ce == 1'b1) begin
                if (enable == 1'b1) begin
                    case (adsr_state)
                        IDLE : adsr_state <= IDLE;

                        ATTACK: begin
                            if (acc == 16'hffff)
                                adsr_state <= DECAY;
                            else
                                acc <= (a_sampled > (16'hffff - acc)) ? 16'hffff : acc + a_sampled;
                        end

                        DECAY: begin
                            if (acc == s_sampled)
                                adsr_state <= SUSTAIN;
                            else
                                acc <= ((acc - d_sampled) < s_sampled) ? s_sampled : acc - d_sampled;
                        end

                        SUSTAIN: begin
                            if (gate == 1'b0) begin
                                adsr_state <= RELEASE;
                            end
                        end

                        RELEASE: begin
                            if (acc == 16'h0000)
                                adsr_state <= IDLE;
                            else
                                acc <= (acc <= r_sampled) ? 16'h0000 : acc - r_sampled;
                        end

                        default: begin
                            acc <= acc;
                        end
                    endcase

                end else
                    adsr_state <= IDLE;
            end
            if (posedge_gate == 1'b1) begin
                adsr_state <= ATTACK;
                a_sampled  <= a;
                d_sampled  <= d;
                s_sampled  <= s;
                r_sampled  <= r;
            end
        end
    end

    assign out = acc;



endmodule
