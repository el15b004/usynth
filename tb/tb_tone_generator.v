`timescale 1ns/100ps

module tb_tone_generator ();

    reg clock;
    reg ce25;
    reg reset;
    wire [15:0] out;

    tone_generator U0 (clock,reset, ce25,16'h000f,out);


    initial begin
        clock     = 1'b0;
        ce25      = 1'b0;
        reset     = 1'b1;
        #10;
        reset     = 1'b0;
        //f = $fopen("output.txt","w");
        #100000 $finish;
    end


    always
          #5 clock = ~clock;

    always
          #20 ce25 = ~ce25;




          initial begin
          $dumpfile("tone_generator.vcd");
          $dumpvars(0,tb_tone_generator);
          $monitor("CLK:%b,reset:%b,OUT:%b",clock,reset,out);

        end



        integer f;
        initial begin
        f = $fopen("out_rect.csv "); if(!f) $finish;
        end
        always @(posedge clock)
        $fdisplay(f, "%d",out);






endmodule
