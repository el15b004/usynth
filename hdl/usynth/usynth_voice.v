module voice   #(parameter WAVEFORM = 0) (input wire clock,
                input wire ce_sample,
                input wire ce_timing,
                input wire reset,
                input wire  [15:0] freq,
                input wire [15:0] a,    // attack
                input wire [15:0] d,    // decay
                input wire [15:0] s,    // sustain
                input wire [15:0] r,    // release
                input wire  play_note,
                output wire [15:0] sample,
                input wire envelope_enable);

    wire [15:0] tone;
    wire [15:0] envelope;
    wire [15:0] sample_envelope;

    tone_generator #(.WAVEFORM(WAVEFORM)) tg (.clock(clock), .reset(reset), .freq(freq),
                                              .ce(ce_sample), .out(tone));

    envelope_generator eg ( .clock(clock), .reset(reset), .enable(envelope_enable),
                            .ce(ce_timing),
                            .a(a),.d(d),.s(s),.r(r),
                            .gate(play_note),
                            .out(envelope));

    mixer mix (.clock(clock), .reset(reset), .ce(ce_sample),
               .tone(tone), .envelope(envelope), .out(sample_envelope));
               
    assign sample = (envelope_enable) ? sample_envelope : tone;

endmodule
