`timescale 1ns/1ps

module tb_spi_try;

	// Inputs
	reg Reset;
  reg SysClk;

	reg SPI_CLK;
	reg SPI_MOSI;
	reg SPI_SS;
	reg [7:0] txMemData;

	// Outputs
	wire SPI_MISO;
	wire [15:0] txMemAddr;
	wire [15:0] rcMemAddr;
	wire [7:0] rcMemData;
	wire rcMemWE;
  wire [7:0] debug_out;

	// Instantiate the Unit Under Test (UUT)
	spi2bus_top uut (
		.reset(Reset),
    .clock(SysClk),
    .ss(SPI_SS),
    .mosi(SPI_MOSI),
		.sck(SPI_CLK),
		.int_address(rcMemAddr),
		.int_wr_data(rcMemData),
		.int_write(rcMemWE)
	);

  task recvByte;
    input   [7:0] rcByte;
    integer       rcBitIndex;
    begin
      $display("%g - spiifc receiving byte '0x%h'", $time, rcByte);
      for (rcBitIndex = 0; rcBitIndex < 8; rcBitIndex = rcBitIndex + 1) begin
        SPI_MOSI = rcByte[7 - rcBitIndex];
        #50;
      end
    end
  endtask

  always begin
    #1 SysClk = ~SysClk;
  end

  reg SPI_CLK_en;
  initial begin
    #300
    SPI_CLK_en = 1;
  end


  always begin
    if (SPI_CLK_en) begin
      #25 SPI_CLK = ~SPI_CLK;
    end else begin
			#25;
		end
  end

  integer fdRcBytes;
  integer fdTxBytes;
  integer dummy;
  integer currRcByte;
  integer rcBytesNotEmpty;
  reg [8*10:1] rcBytesStr;

	initial begin
		// Initialize Inputs
		Reset = 1;
    SysClk = 0;

		SPI_CLK = 0;
    SPI_CLK_en = 0;
		SPI_MOSI = 0;
		SPI_SS = 0;
		txMemData = 0;

		// Wait 100 ns for global reset to finish
		#200;
    //Reset = 1;

    #100;
    Reset = 0;

    //#100;

		// Add stimulus here
    SPI_SS = 0;
    // For each byte, transmit its bits
    fdRcBytes = $fopen("rc-bytes-rc-test.txt", "r");
    rcBytesNotEmpty = $fgets(rcBytesStr, fdRcBytes);
    while (rcBytesNotEmpty) begin
      if (rcBytesNotEmpty) begin
        dummy = $sscanf(rcBytesStr, "%x", currRcByte);
        recvByte(currRcByte);
			end
			rcBytesNotEmpty = $fgets(rcBytesStr, fdRcBytes);
    end

    // Wrap it up.
    SPI_SS = 1;
    #1000;

    $finish;
	end

  initial begin
  $display("clk out");
  $dumpfile("spi2bus.vcd");
  $dumpvars(0,tb_spi_try);
  //$dumpvars(0,tb_midi.U0.oscillator[0]);//schleife
  //$monitor("CLK:%b,reset:%b",clock,reset);
end

endmodule
